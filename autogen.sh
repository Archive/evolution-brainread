#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="evolution-brainread"

(test -f $srcdir/configure.in \
  && test -f $srcdir/src/GNOME_Evolution_BrainRead.server.in.in) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

# Due to a bug in gnome-autogen, if we don't put this, it will only
# accept automake-1.4. But with this it will accept 1.6 or 1.7
REQUIRED_AUTOMAKE_VERSION=1.6

which gnome-autogen.sh || {
    echo "You need to install gnome-common from GNOME CVS"
    exit 1
}
USE_GNOME2_MACROS=1 . gnome-autogen.sh
