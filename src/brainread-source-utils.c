/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "brainread-source-utils.h"
#include "brainread-utils.h"
#include "Evolution-Addressbook-SelectNames.h"

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-widget.h>
#include <glade/glade-xml.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkdialog.h>
#include <libgnome/gnome-i18n.h>
#include <shell/Evolution.h>

#define SELECT_NAMES_OAFIID "OAFIID:GNOME_Evolution_Addressbook_SelectNames:" EVOLUTION_API_VERSION

ESourceList *
brainread_source_list_new (void)
{
	ESourceList *source_list;
	ESourceGroup *group;

	source_list = e_source_list_new_for_gconf_default ("/apps/evolution/brainread/sources");

	if (!e_source_list_peek_groups (source_list)) {
		/* Create the default group */
		group = e_source_group_new (_("Blogs"),
					    "brainread://Blogs");
		e_source_list_add_group (source_list, group, 0);
		e_source_list_sync (source_list, NULL);
	}

	return source_list;
}


typedef struct {
	ESourceGroup *source_group;
	GtkDialog *dialog;
	GNOME_Evolution_Addressbook_SelectNames select_names;
	BonoboWidget *entry;
} BrainReadAddBloggerData;

static void
dialog_response (GtkDialog *dialog, int response_id, gpointer data)
{
	BrainReadAddBloggerData *abd = data;
	ESourceGroup *source_group;
	ESource *new_source;
	char *dests = NULL, *source_uri;
	xmlChar *book_uri = NULL, *uid = NULL, *name = NULL;
	xmlNode *top, *node;
	xmlDoc *doc;

	if (response_id == GTK_RESPONSE_OK) {
		/* Get the XML data describing the selected user */
		bonobo_widget_get_property (abd->entry, "destinations",
					    TC_CORBA_string, &dests,
					    NULL);
	}
	bonobo_object_release_unref (abd->select_names, NULL);
	gtk_widget_destroy (GTK_WIDGET (dialog));

	source_group = abd->source_group;
	g_free (abd);

	if (!dests) {
		g_object_unref (source_group);
		return;
	}

	/* Parse it and extract the info we need: we ought to be able
	 * to use eab-destination for this, but it's (mistakenly)
	 * internal to evolution, so we have to do it by hand.
	 */
	doc = xmlParseDoc (dests);
	if (!doc)
		goto done;

	top = brainread_xml_find_next (doc->children, NULL, "destination", FALSE);
	if (!top)
		goto done;

	node = brainread_xml_find_next (top, top, "book_uri", FALSE);
	if (node)
		book_uri = xmlNodeGetContent (node);
	node = brainread_xml_find_next (top, top, "card_uid", FALSE);
	if (node)
		uid = xmlNodeGetContent (node);
	node = brainread_xml_find_next (top, top, "name", FALSE);
	if (node)
		name = xmlNodeGetContent (node);

	if (!uid)
		goto done;

	if (!book_uri) {
		/* Currently (2004-03-23), the select-names code
		 * doesn't actually return the book_uri. :-(
		 * We kludge and use "" to mean "the default book".
		 */
		book_uri = xmlStrdup ("");
	}

	source_uri = g_strdup_printf ("%s#%s", book_uri, uid);
	new_source = e_source_new (name ? (char *) name : "???", source_uri);
	/* FIXME: check -1 */
	e_source_group_add_source (source_group, new_source, -1);
	g_object_unref (new_source);
	g_free (source_uri);

 done:
	if (doc)
		xmlFreeDoc (doc);
	if (book_uri)
		xmlFree (book_uri);
	if (uid)
		xmlFree (uid);
	if (name)
		xmlFree (name);
	g_object_unref (source_group);
}

static void
addressbook_clicked (GtkWidget *widget, gpointer data)
{
	BrainReadAddBloggerData *abd = data;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	GNOME_Evolution_Addressbook_SelectNames_activateDialog (
		abd->select_names, "blogger", &ev);
	CORBA_exception_free (&ev);
}

void
brainread_source_add (GtkWindow *window, ESourceGroup *source_group)
{
	BrainReadAddBloggerData *abd;
	GNOME_Evolution_Addressbook_SelectNames select_names;
	Bonobo_Control corba_control;
	CORBA_Environment ev;
	GtkWidget *dialog;
	GtkWidget *hbox, *button, *entry;
	GladeXML *glade;

	/* Get a "SelectNames" control from the addressbook */
	CORBA_exception_init (&ev);

	select_names = bonobo_activation_activate_from_id (SELECT_NAMES_OAFIID, 0, NULL, &ev);
	if (BONOBO_EX (&ev)) {
		/* This leaks memory (the results of
		 * bonobo_exception_get_text), but it's not supposed to
		 * happen, so we don't care.
		 */
		g_warning ("brainread_source_add: %s",
			   bonobo_exception_get_text (&ev));
		return;
	}

	GNOME_Evolution_Addressbook_SelectNames_addSectionWithLimit (
		select_names, "user", _("User"), 1, &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("brainread_source_add: %s",
			   bonobo_exception_get_text (&ev));
		bonobo_object_release_unref (select_names, NULL);
		return;
	}

	corba_control = GNOME_Evolution_Addressbook_SelectNames_getEntryBySection (select_names, "user", &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("brainread_source_add: %s",
			   bonobo_exception_get_text (&ev));
		bonobo_object_release_unref (select_names, NULL);
		return;
	}

	CORBA_exception_free (&ev);

	/* Get the dialog from glade */
	glade = glade_xml_new (BRAINREAD_GLADEDIR "/brainread.glade",
			       "add_blogger", GETTEXT_PACKAGE);
	if (!glade) {
		g_warning ("brainread_source_add: Could not find brainread.glade");
		bonobo_object_release_unref (select_names, NULL);
		return;
	}

	dialog = glade_xml_get_widget (glade, "add_blogger");
	hbox = glade_xml_get_widget (glade, "hbox");
	button = glade_xml_get_widget (glade, "abook_btn");
	g_object_ref (dialog);
	g_object_unref (glade);

	abd = g_new0 (BrainReadAddBloggerData, 1);
	abd->select_names = select_names;
	abd->source_group = g_object_ref (source_group);
	/* (We ref the source_group since otherwise it could [at least
	 * theoretically] be destroyed before the callback was called.
	 */

	if (GTK_IS_WINDOW (window)) {
		/* Tell the window manager that the new dialog is
		 * associated with the given window.
		 */
		gtk_window_set_transient_for (GTK_WINDOW (abd->dialog), window);
	}

	/* Add the select-names entry to the dialog */
	entry = bonobo_widget_new_control_from_objref (corba_control,
						       CORBA_OBJECT_NIL);
	gtk_widget_show (entry);
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
	abd->entry = BONOBO_WIDGET (entry);

	/* Connect to the "Addressbook" button */
	g_signal_connect (button, "clicked",
			  G_CALLBACK (addressbook_clicked), abd);

	/* And the other buttons */
	g_signal_connect (dialog, "response",
			  G_CALLBACK (dialog_response), abd);

	/* And run */
	gtk_widget_show (dialog);
}

