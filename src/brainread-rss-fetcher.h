/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_RSS_FETCHER_H__
#define __BRAINREAD_RSS_FETCHER_H__

/* This header is almost entirely GObject-related boilerplate.
 * Generally when writing a new class, you would copy+paste from
 * an existing class and then find+replace.
 */

#include <glib-object.h>
#include <libsoup/soup-message.h>

#include "brainread-rss.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

/* The standard GObject macros:
 *
 * BRAINREAD_TYPE_RSS_FETCHER: returns the GType for BrainReadRSSFetcher
 * BRAINREAD_RSS_FETCHER(obj): casts "obj" to BrainReadRSSFetcher *,
 *   emitting a warning and returning NULL if obj is NULL or points to a
 *   GObject that is not a BrainReadRSSFetcher.
 * BRAINREAD_RSS_FETCHER_CLASS(klass): likewise, to cast a class object
 *   to BrainReadRSSFetcherClass *. (We say "klass" here and elsewhere
 *   because "class" would cause syntax errors in C++.)
 * BRAINREAD_IS_RSS_FETCHER(obj): tests if a GObject is a
 *   BrainReadRSSFetcher.
 * BRAINREAD_IS_RSS_FETCHER_CLASS(obj): likewise, tests if a class object
 *   is a BrainReadRSSFetcherClass.
 * BRAINREAD_RSS_FETCHER_GET_CLASS(obj): given an object, extract its
 *   class as a BrainReadRSSFetcherClass. Used to implement virtual
 *   methods. (See brainread-rss-fetcher.c.)
 *
 * Note the use of namespacing: the standard naming scheme assumes
 * that each class name has a namespace ("Brainread") and a name
 * inside that namespace ("RSSFetcher").
 */

#define BRAINREAD_TYPE_RSS_FETCHER            (brainread_rss_fetcher_get_type ())
#define BRAINREAD_RSS_FETCHER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAINREAD_TYPE_RSS_FETCHER, BrainReadRSSFetcher))
#define BRAINREAD_RSS_FETCHER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BRAINREAD_TYPE_RSS_FETCHER, BrainReadRSSFetcherClass))
#define BRAINREAD_IS_RSS_FETCHER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAINREAD_TYPE_RSS_FETCHER))
#define BRAINREAD_IS_RSS_FETCHER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BRAINREAD_TYPE_RSS_FETCHER))
#define BRAINREAD_RSS_FETCHER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), BRAINREAD_TYPE_RSS_FETCHER, BrainReadRSSFetcherClass))

typedef struct BrainReadRSSFetcher        BrainReadRSSFetcher;
typedef struct BrainReadRSSFetcherPrivate BrainReadRSSFetcherPrivate;
typedef struct BrainReadRSSFetcherClass   BrainReadRSSFetcherClass;


/* Define the BrainReadRSSFetcher structure. Note that the "parent"
 * field is a GObject structure, *NOT* a pointer to a GObject.
 * This means that a BrainReadRSSFetcher * can be cast to a
 * GObject *.
 *
 * If BrainReadRSSFetcher had any public fields, they would also be
 * listed here. We use the convention of just having a "priv" pointer
 * pointing to private data, so that other code can't muck up our
 * internal state. Also, that lets us add or change fields later
 * without breaking ABI compatibility by changing the size of the
 * structure.
 */
struct BrainReadRSSFetcher {
	GObject parent;
	
	BrainReadRSSFetcherPrivate *priv;
};

/* Likewise, this defines the class structure. Any signals or
 * methods implemented by the class will be prototyped here.
 * (There's no way to tell just from looking at the prototype
 * whether something is a signal or a method, so it's customary
 * to label them with a comment.)
 *
 * If you wanted to have the equivalent of static class members,
 * you could also include them in the class structure, but this
 * is not commonly done.
 */
struct BrainReadRSSFetcherClass {
	GObjectClass parent_class;

	/* signals */
	void (*fetched_data) (BrainReadRSSFetcher *, SoupMessage *,
			      BrainReadRSS *, GError *);

	/* methods */
	void (*fetch) (BrainReadRSSFetcher *);
};

/* The standard type-getting macro. Outside code may assume that
 * it has exactly this name format, though technically, they should
 * be using BRAINREAD_RSS_FETCHER_TYPE rather than calling this
 * directly.
 */
GType brainread_rss_fetcher_get_type (void);


/* A "constructor" and some methods. */
BrainReadRSSFetcher *brainread_rss_fetcher_new   (SoupSession *session,
						  const char  *rss_url);

void                 brainread_rss_fetcher_fetch (BrainReadRSSFetcher *rss_fetcher);

const char          *brainread_rss_fetcher_url   (BrainReadRSSFetcher *rss_fetcher);


/* Error handling */
GQuark brainread_rss_fetcher_error_quark (void);
#define BRAINREAD_RSS_FETCHER_ERROR brainread_rss_fetcher_error_quark()
enum {
	BRAINREAD_RSS_FETCHER_ERROR_HTTP
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_RSS_FETCHER_H__ */
