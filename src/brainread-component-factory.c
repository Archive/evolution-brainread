/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-component-factory.c
 *
 * This initializes the libevolution-brainread.so component and sets
 * up a CORBA "factory" for creating BrainreadComponent objects. Most
 * of the work is done by the BONOBO_ACTIVATION_SHLIB_FACTORY macro,
 * which creates the registration function that will be called when
 * another component uses bonobo-activation to activate this
 * component. That registration function will declare that the
 * brainread_factory() function can be called to create a new
 * BrainreadComponent object.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "brainread-component.h"

#include <string.h>
#include <bonobo/bonobo-shlib-factory.h>

#define BRAINREAD_FACTORY_ID   "OAFIID:GNOME_Evolution_BrainRead_Factory:" EVOLUTION_API_VERSION
#define BRAINREAD_COMPONENT_ID "OAFIID:GNOME_Evolution_BrainRead_Component:" EVOLUTION_API_VERSION

static BonoboObject *
brainread_factory (BonoboGenericFactory *factory,
		   const char           *component_id,
		   gpointer              user_data)
{
	/* Make sure it's asking for what we expected. */
	if (strcmp (component_id, BRAINREAD_COMPONENT_ID) != 0) {
		g_warning (BRAINREAD_FACTORY_ID ": Dont't know how to activate %s", component_id);
		return NULL;
	}

	return g_object_new (BRAINREAD_TYPE_COMPONENT, NULL);
}

BONOBO_ACTIVATION_SHLIB_FACTORY (BRAINREAD_FACTORY_ID, "BrainRead component factory", brainread_factory, NULL)
