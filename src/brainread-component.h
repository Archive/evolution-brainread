/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_COMPONENT_H__
#define __BRAINREAD_COMPONENT_H__

/* This header is basically all GObject/BonoboObject-related
 * boilerplate. Basically all we are doing is declaring a subclass of
 * BonoboObject that implements the GNOME::Evolution::Component
 * interface. See brainread-rss-fetcher.h for more details of the
 * GObject-specific parts.
 */

#include <bonobo/bonobo-object.h>
#include <shell/Evolution.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define BRAINREAD_TYPE_COMPONENT            (brainread_component_get_type ())
#define BRAINREAD_COMPONENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAINREAD_TYPE_COMPONENT, BrainReadComponent))
#define BRAINREAD_COMPONENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BRAINREAD_TYPE_COMPONENT, BrainReadComponentClass))
#define BRAINREAD_IS_COMPONENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAINREAD_TYPE_COMPONENT))
#define BRAINREAD_IS_COMPONENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BRAINREAD_TYPE_COMPONENT))

typedef struct BrainReadComponent           BrainReadComponent;
typedef struct BrainReadComponentPrivate    BrainReadComponentPrivate;
typedef struct BrainReadComponentClass      BrainReadComponentClass;

struct BrainReadComponent {
	BonoboObject parent;
	
	BrainReadComponentPrivate *priv;
};

struct BrainReadComponentClass {
	BonoboObjectClass parent_class;

	/* This is part of the BonoboObject boilerplate. */
	POA_GNOME_Evolution_Component__epv epv;
};

GType brainread_component_get_type (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_COMPONENT_H__ */
