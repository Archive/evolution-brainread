/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_SOURCE_UTILS_H__
#define __BRAINREAD_SOURCE_UTILS_H__

#include <gtk/gtkwindow.h>
#include <libedataserver/e-source-list.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

ESourceList *brainread_source_list_new (void);

void         brainread_source_add      (GtkWindow    *window,
					ESourceGroup *source_group);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_UTILS_H__ */
