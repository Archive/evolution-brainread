/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_BLOG_FETCHER_H__
#define __BRAINREAD_BLOG_FETCHER_H__

/* BrainReadBlogFetcher is a subclass of BrainReadRSSFetcher
 * that specifically handles finding the blog for a person.
 */

#include "brainread-rss-fetcher.h"
#include <libebook/e-contact.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define BRAINREAD_TYPE_BLOG_FETCHER            (brainread_blog_fetcher_get_type ())
#define BRAINREAD_BLOG_FETCHER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAINREAD_TYPE_BLOG_FETCHER, BrainReadBlogFetcher))
#define BRAINREAD_BLOG_FETCHER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BRAINREAD_TYPE_BLOG_FETCHER, BrainReadBlogFetcherClass))
#define BRAINREAD_IS_BLOG_FETCHER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAINREAD_TYPE_BLOG_FETCHER))
#define BRAINREAD_IS_BLOG_FETCHER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BRAINREAD_TYPE_BLOG_FETCHER))

typedef struct BrainReadBlogFetcher        BrainReadBlogFetcher;
typedef struct BrainReadBlogFetcherPrivate BrainReadBlogFetcherPrivate;
typedef struct BrainReadBlogFetcherClass   BrainReadBlogFetcherClass;

/* BrainReadRSSFetcher is our base class */
struct BrainReadBlogFetcher {
	BrainReadRSSFetcher parent;
	
	BrainReadBlogFetcherPrivate *priv;
};

struct BrainReadBlogFetcherClass {
	BrainReadRSSFetcherClass parent_class;

};

GType brainread_blog_fetcher_get_type (void);

/* Note that we return a BrainReadRSSFetcher rather than a
 * BrainReadBlogFetcher. This is because the caller is likely to
 * want to use it as a BrainReadRSSFetcher. Gtk does the same
 * thing with widgets (virtually all widget constructors return
 * a GtkWidget *, not a GtkVBox *, etc).
 */
BrainReadRSSFetcher *brainread_blog_fetcher_new (SoupSession *session,
						 EContact    *contact);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_BLOG_FETCHER_H__ */
