/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-rss.c
 *
 * Routines to parse RSS data, and create and destroy RSS-related
 * data structures.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "brainread-rss.h"
#include "brainread-utils.h"

#include <string.h>

#include <glib.h>
#include <libgnome/gnome-i18n.h>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

static gboolean
string_is_set (char **string, gboolean validate)
{
	/* Make sure *string is set. If it's not, either set it
	 * or return an error, depending on the value of
	 * validate.
	 */
	if (!*string) {
		if (validate)
			return FALSE;
		*string = g_strdup ("");
	}
	return TRUE;
}

BrainReadRSS *
brainread_rss_new (const char *rss_data, int len,
		   gboolean validate, GError **err)
{
	xmlDoc *doc;
	xmlNode *node, *channel, *inode;
	xmlChar *tmp;
	BrainReadRSS *rss;
	BrainReadRSSItem *item;

	/* libxml provides two interfaces, the DOM interface, where it
	 * returns a complete tree all at once, and the SAX interface,
	 * where you give it a bunch of callback functions and it
	 * calls them as it parses various parts of the input. We use
	 * the DOM interface here, since it's much easier to deal
	 * with, and RSS feeds should never be so huge that we would
	 * mind having the whole tree in memory at once.
	 *
	 * xmlParseMemory does validation and will return NULL if the
	 * input data is not strictly valid XML. xmlRecoverMemory does
	 * not.
	 */

	if (validate)
		doc = xmlParseMemory (rss_data, len);
	else
		doc = xmlRecoverMemory (rss_data, len);

	if (!doc) {
		g_set_error (err, BRAINREAD_RSS_ERROR,
			     BRAINREAD_RSS_ERROR_NOT_RSS,
			     _("Data is not valid XML"));
		return NULL;
	}

	node = brainread_xml_find_next (doc->children, NULL, "rss", FALSE);
	if (!node) {
		g_set_error (err, BRAINREAD_RSS_ERROR,
			     BRAINREAD_RSS_ERROR_NOT_RSS,
			     _("Data is XML, but not RSS"));
		return NULL;
	}

	/* Find the channel node. */
	channel = brainread_xml_find_next (node, node, "channel", FALSE);
	if (!channel) {
		g_set_error (err, BRAINREAD_RSS_ERROR,
			     BRAINREAD_RSS_ERROR_NOT_RSS,
			     _("No <channel> node in RSS data"));
		return NULL;
	}

	rss = g_new0 (BrainReadRSS, 1);
	rss->items = g_ptr_array_new ();

	node = brainread_xml_find_next (channel, channel, "title", FALSE);
	if (node)
		rss->title = xmlNodeGetContent (node);

	node = brainread_xml_find_next (channel, channel, "link", FALSE);
	if (node)
		rss->link = xmlNodeGetContent (node);

	node = brainread_xml_find_next (channel, channel, "description", FALSE);
	if (node)
		rss->description = xmlNodeGetContent (node);

	/* Integrity checks. */
	if (!string_is_set (&rss->title, validate) ||
	    !string_is_set (&rss->link, validate) ||
	    !string_is_set (&rss->description, validate)) {
		g_set_error (err, BRAINREAD_RSS_ERROR,
			     BRAINREAD_RSS_ERROR_BAD_RSS,
			     _("RSS data is missing one or more required "
			       "<channel> elements"));
		goto fail;
	}

	node = brainread_xml_find_next (channel, channel, "image", FALSE);
	if (node) {
		inode = brainread_xml_find_next (node, node, "title", FALSE);
		if (inode)
			rss->image_title = xmlNodeGetContent (inode);

		inode = brainread_xml_find_next (node, node, "link", FALSE);
		if (inode)
			rss->image_link = xmlNodeGetContent (inode);

		inode = brainread_xml_find_next (node, node, "url", FALSE);
		if (inode)
			rss->image_url = xmlNodeGetContent (inode);

		inode = brainread_xml_find_next (node, node, "description", FALSE);
		if (inode)
			rss->image_description = xmlNodeGetContent (inode);

		inode = brainread_xml_find_next (node, node, "width", FALSE);
		if (inode) {
			tmp = xmlNodeGetContent (inode);
			rss->image_width = atoi (tmp);
			xmlFree (tmp);
		}

		inode = brainread_xml_find_next (node, node, "height", FALSE);
		if (inode) {
			tmp = xmlNodeGetContent (inode);
			rss->image_height = atoi (tmp);
			xmlFree (tmp);
		}

		if (!string_is_set (&rss->image_url, validate) ||
		    !string_is_set (&rss->image_title, validate) ||
		    !string_is_set (&rss->image_link, validate)) {
			g_set_error (err, BRAINREAD_RSS_ERROR,
				     BRAINREAD_RSS_ERROR_BAD_RSS,
				     _("RSS data is missing one or more required "
				       "elements of the <image> node"));
			goto fail;
		}
	}

	for (node = brainread_xml_find_next (channel, channel, "item", FALSE);
	     node;
	     node = brainread_xml_find_next (node, channel, "item", FALSE)) {
		item = g_new0 (BrainReadRSSItem, 1);
		g_ptr_array_add (rss->items, item);

		inode = brainread_xml_find_next (node, node, "title", FALSE);
		if (inode)
			item->title = xmlNodeGetContent (inode);
		inode = brainread_xml_find_next (node, node, "link", FALSE);
		if (inode)
			item->link = xmlNodeGetContent (inode);
		inode = brainread_xml_find_next (node, node, "description", FALSE);
		if (inode)
			item->description = xmlNodeGetContent (inode);

		if (!string_is_set (&item->title, validate) ||
		    !string_is_set (&item->description, validate)) {
			g_set_error (err, BRAINREAD_RSS_ERROR,
				     BRAINREAD_RSS_ERROR_BAD_RSS,
				     _("RSS item found with neither title nor description"));
			goto fail;
		}
	}

	xmlFreeDoc (doc);
	return rss;

 fail:
	/* We end up here if there was a parse error */
	xmlFreeDoc (doc);
	brainread_rss_free (rss);
	return NULL;
}

char *
brainread_rss_to_html (BrainReadRSS *rss)
{
	GString *html;
	char *ret;
	int i;

	/* First the header... perhaps we could add a CSS stylesheet? */
	html = g_string_new ("<html>\n");
	g_string_append (html, "<head>\n<title>");
	brainread_append_to_html (html, rss->title);
	g_string_append (html, "</title>\n");
	g_string_append (html, "</head><body>\n");

	/* If there is an image, build a table to contain the image
	 * and the title, and write out the information for the image.
	 */
	if (rss->image_url) {
		g_string_append (html, "<table><tr><td><a href=\"");
		brainread_append_to_html (html, rss->image_link);
		g_string_append (html, "\"");
		if (rss->image_description) {
			g_string_append (html, " alt=\"");
			brainread_append_to_html (html, rss->image_description);
			g_string_append (html, "\"");
		}
		g_string_append (html, "><img src=\"");
		brainread_append_to_html (html, rss->image_url);
		g_string_append (html, "\" alt=\"");
		brainread_append_to_html (html, rss->image_title);
		g_string_append (html, "\"");
		if (rss->image_width > 0 && rss->image_height > 0) {
			g_string_append_printf (html, " width=%d height=%d",
						rss->image_width,
						rss->image_height);
		}
		g_string_append (html, "></td><td>");
	}

	/* Now do the title */
	g_string_append (html, "<h1>");
	g_string_append (html, "<a href=\"");
	brainread_append_to_html (html, rss->link);
	g_string_append (html, "\">");
	brainread_append_to_html (html, rss->title);
	g_string_append (html, "</a>");
	g_string_append (html, "</h1>");

	/* And close off the table if there was one */
	if (rss->image_url)
		g_string_append (html, "</td></tr></table>");
	g_string_append_c (html, '\n');

	/* The description, as a subheading */
	g_string_append (html, "<h2>");
	brainread_append_to_html (html, rss->description);
	g_string_append (html, "</h2>\n\n");

	/* And finally the items, in a "description list". */
	g_string_append (html, "<dl>\n");
	for (i = 0; i < rss->items->len; i++) {
		BrainReadRSSItem *item = rss->items->pdata[i];

		g_string_append (html, "  <dt><a href=\"");
		brainread_append_to_html (html, item->link);
		g_string_append (html, "\">");
		brainread_append_to_html (html, item->title);
		g_string_append (html, "</a></dt>\n");
		g_string_append (html, "  <dd><p>");
		g_string_append (html, item->description);
		g_string_append (html, "</p></dd>\n\n");
	}
	g_string_append (html, "</dl>\n\n");

	g_string_append (html, "</body>\n</html>\n");

	ret = html->str;
	/* Pass FALSE to g_string_free to tell it to free the GString
	 * structure, but not the string itself.
	 */
	g_string_free (html, FALSE);
	return ret;
}

void
brainread_rss_free (BrainReadRSS *rss)
{
	BrainReadRSSItem *item;
	int i;

	/* It's safe to call xmlFree with NULL */

	xmlFree (rss->title);
	xmlFree (rss->link);
	xmlFree (rss->description);

	xmlFree (rss->image_title);
	xmlFree (rss->image_link);
	xmlFree (rss->image_url);

	for (i = 0; i < rss->items->len; i++) {
		item = rss->items->pdata[i];
		xmlFree (item->title);
		xmlFree (item->link);
		xmlFree (item->description);
		g_free (item);
	}
	g_ptr_array_free (rss->items, TRUE);

	g_free (rss);
}

GQuark
brainread_rss_error_quark (void)
{
	static GQuark error_quark;

	if (!error_quark)
		error_quark = g_quark_from_static_string ("BRAINREAD_RSS_ERROR");
	return error_quark;
}
