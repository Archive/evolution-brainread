/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_RSS_H__
#define __BRAINREAD_RSS_H__

#include <glib.h>
#include <libxml/parser.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

/* Structures to implement the data in the RSS 2.0 specification,
 * currently hosted at http://blogs.law.harvard.edu/tech/rss
 *
 * Note that we ignore many of the fields.
 */

typedef struct {
	/* Title text and the URL it should link to */
	char *title;
	char *link;

	/* Description of the channel */
	char *description;

	/* Image to go with the channel */
	char *image_url;
	char *image_title;
	char *image_link;
	char *image_description;
	int image_width, image_height;

	/* The <item>s */
	GPtrArray *items;
} BrainReadRSS;

typedef struct {
	/* The item's title */
	char *title;

	/* The link to the full article */
	char *link;

	/* A brief abstract of the article */
	char *description;
} BrainReadRSSItem;


/* Error handling */
GQuark brainread_rss_error_quark (void);
#define BRAINREAD_RSS_ERROR brainread_rss_error_quark()
enum {
	BRAINREAD_RSS_ERROR_NOT_RSS,
	BRAINREAD_RSS_ERROR_BAD_RSS
};


BrainReadRSS *brainread_rss_new         (const char    *rss_data,
					 int            length,
					 gboolean       validate,
					 GError       **err);

char         *brainread_rss_to_html     (BrainReadRSS  *rss);

void          brainread_rss_free        (BrainReadRSS  *rss);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_RSS_H__ */
