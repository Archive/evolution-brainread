/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_UTILS_H__
#define __BRAINREAD_UTILS_H__

#include <glib.h>
#include <libxml/tree.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

char       *brainread_base64_encode  (const guint8 *data,
				      int           length);
GByteArray *brainread_base64_decode  (const char   *string);

xmlNode    *brainread_xml_find_next  (xmlNode      *start,
				      xmlNode      *top,
				      const char   *name,
				      gboolean      case_insensitive);

void        brainread_append_to_html (GString      *str,
				      const char   *text);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_UTILS_H__ */
