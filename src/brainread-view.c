/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-view.c
 *
 * This manages a view of the BrainRead component. There will be one
 * of these for each open Evolution window in which the BrainRead
 * component is being viewed.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "brainread-view.h"
#include "brainread-blog-fetcher.h"
#include "brainread-source-utils.h"
#include "brainread-utils.h"

#include <string.h>

#include <e-util/e-dialog-utils.h>
#include <gal/util/e-util.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtkhtml/gtkhtml.h>
#include <libebook/e-book.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-url.h>
#include <shell/e-user-creatable-items-handler.h>
#include <widgets/e-activity-handler.h>
#include <widgets/e-source-selector.h>
#include <widgets/e-task-bar.h>

struct BrainReadViewPrivate {
	SoupSession      *session;
	GHashTable       *rss_fetchers;
	ESourceList      *source_list;
	EActivityHandler *activity_handler;
	EUserCreatableItemsHandler *creatable_items_handler;

	ESourceSelector  *source_selector;
	BonoboControl    *sidebar_control;

	GtkHTML          *html;
	BonoboControl    *view_control;

	ETaskBar         *task_bar;
	BonoboControl    *statusbar_control;

	GSList           *msgs;
};

/* Properties */
enum {
	PROP_SESSION = 1,
	PROP_SOURCE_LIST,
	LAST_PROP
};

static void set_property (GObject *object, guint prop_id,
			  const GValue *value, GParamSpec *pspec);
static void get_property (GObject *object, guint prop_id,
			  GValue *value, GParamSpec *pspec);

#define PARENT_TYPE G_TYPE_OBJECT
static GObjectClass *parent_class = NULL;

static void create_blog (EUserCreatableItemsHandler *handler, const char *item_type_name, gpointer data);

static void
init (GObject *object)
{
	BrainReadView *view = BRAINREAD_VIEW (object);
	BrainReadViewPrivate *priv;

	priv = view->priv = g_new0 (BrainReadViewPrivate, 1);
	priv->rss_fetchers = g_hash_table_new_full (g_str_hash, g_str_equal,
						    g_free, g_object_unref);
	priv->creatable_items_handler = e_user_creatable_items_handler_new ("brainread", create_blog, view);
}

static void
dispose (GObject *object)
{
	BrainReadView *view = BRAINREAD_VIEW (object);
	BrainReadViewPrivate *priv = view->priv;

	while (priv->msgs) {
		SoupMessage *msg;

		msg = priv->msgs->data;
		priv->msgs = g_slist_remove (priv->msgs, msg);
		soup_message_set_status (msg, SOUP_STATUS_CANCELLED);
		soup_session_cancel_message (priv->session, msg);
	}

	if (priv->session) {
		g_object_unref (priv->session);
		priv->session = NULL;
	}
	if (priv->rss_fetchers) {
		g_hash_table_destroy (priv->rss_fetchers);
		priv->rss_fetchers = NULL;
	}
	if (priv->source_list) {
		g_object_unref (priv->source_list);
		priv->source_list = NULL;
	}
	if (priv->activity_handler) {
		g_object_unref (priv->activity_handler);
		priv->activity_handler = NULL;
	}

	if (priv->sidebar_control) {
		bonobo_object_unref (priv->sidebar_control);
		priv->sidebar_control = NULL;
	}
	if (priv->view_control) {
		bonobo_object_unref (priv->view_control);
		priv->view_control = NULL;
	}
	if (priv->statusbar_control) {
		bonobo_object_unref (priv->statusbar_control);
		priv->statusbar_control = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
finalize (GObject *object)
{
	BrainReadView *view = BRAINREAD_VIEW (object);

	g_free (view->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
class_init (GObjectClass *object_class)
{
	parent_class = g_type_class_peek_parent (object_class);

	/* Override some GObject methods */
	object_class->dispose = dispose;
	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	/* Define our properties */
	g_object_class_install_property (
		object_class, PROP_SESSION,
		g_param_spec_object ("session", "Session",
				     "The SoupSession to use for HTTP",
				     SOUP_TYPE_SESSION,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (
		object_class, PROP_SOURCE_LIST,
		g_param_spec_object ("source_list", "Source list",
				     "The ESourceList of blog sourcers",
				     E_TYPE_SOURCE_LIST,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

E_MAKE_TYPE (brainread_view, "BrainReadView", BrainReadView, class_init, init, PARENT_TYPE)

static void
set_property (GObject *object, guint prop_id,
	      const GValue *value, GParamSpec *pspec)
{
	BrainReadView *view = BRAINREAD_VIEW (object);

	switch (prop_id) {
	case PROP_SESSION:
		view->priv->session = (SoupSession *)g_value_dup_object (value);
		break;
	case PROP_SOURCE_LIST:
		view->priv->source_list = (ESourceList *)g_value_dup_object (value);
		break;
	default:
		break;
	}
}

static void
get_property (GObject *object, guint prop_id,
	      GValue *value, GParamSpec *pspec)
{
	BrainReadView *view = BRAINREAD_VIEW (object);

	switch (prop_id) {
	case PROP_SESSION:
		g_value_set_object (value, view->priv->session);
		break;
	case PROP_SOURCE_LIST:
		g_value_set_object (value, view->priv->source_list);
		break;
	default:
		break;
	}
}


BonoboControl *
brainread_view_peek_sidebar (BrainReadView *view)
{
	g_return_val_if_fail (BRAINREAD_IS_VIEW (view), NULL);

	return view->priv->sidebar_control;
}

BonoboControl *
brainread_view_peek_view (BrainReadView *view)
{
	g_return_val_if_fail (BRAINREAD_IS_VIEW (view), NULL);

	return view->priv->view_control;
}

BonoboControl *
brainread_view_peek_statusbar (BrainReadView *view)
{
	g_return_val_if_fail (BRAINREAD_IS_VIEW (view), NULL);

	return view->priv->statusbar_control;
}


static void
create_blog (EUserCreatableItemsHandler *handler, const char *item_type_name,
	     gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;
	ESourceGroup *blogs_group;
	GtkWidget *window;

	if (strcmp (item_type_name, "blogger") != 0)
		return;

	/* Evolution doesn't currently have a widget to let the user
	 * pick a group from an ESourceList, so we just only work with
	 * one group for now.
	 */
	blogs_group = e_source_list_peek_group_by_name (priv->source_list,
							_("Blogs"));
	g_return_if_fail (blogs_group != NULL);

	window = gtk_widget_get_toplevel (GTK_WIDGET (priv->html));
	brainread_source_add ((GtkWindow *)window, blogs_group);
}


static void
view_activated (BonoboControl *control, gboolean activated, gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;
	BonoboUIComponent *uic;
	Bonobo_UIContainer container;

	uic = bonobo_control_get_ui_component (control);

	if (activated) {
		container = bonobo_control_get_remote_ui_container (control, NULL);
		bonobo_ui_component_set_container (uic, container, NULL);
		bonobo_object_release_unref (container, NULL);
		e_user_creatable_items_handler_activate (priv->creatable_items_handler, uic);
	} else
		bonobo_ui_component_unset_container (uic, NULL);
}


static void
fetched_data (BrainReadRSSFetcher *fetcher, SoupMessage *msg,
	      BrainReadRSS *rss, GError *err, gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;
	GtkHTMLStream *stream;
	char *output;

	e_activity_handler_unset_message (priv->activity_handler);

	if (rss)
		output = brainread_rss_to_html (rss);
	else {
		/* Write out some error text */
		GString *errmsg;

		errmsg = g_string_new ("<html><body><h1>");
		g_string_append (errmsg, _("Error"));
		g_string_append (errmsg, "</h1>\n<p>");
		g_string_append_printf (errmsg, _("An error occurred while trying to display %s:"),
					brainread_rss_fetcher_url (fetcher));
		g_string_append (errmsg, "</p>\n<pre>");
		brainread_append_to_html (errmsg, err->message);
		g_string_append (errmsg, "</pre>\n</body>\n</html>");

		output = errmsg->str;
		g_string_free (errmsg, FALSE);
	}

	stream = gtk_html_begin (priv->html);
	gtk_html_write (priv->html, stream, output, strlen (output));
	gtk_html_end (priv->html, stream, GTK_HTML_STREAM_OK);
	g_free (output);
}

static void
source_selected (ESourceSelector *source_selector, gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;
	BrainReadRSSFetcher *rss_fetcher;
	ESource *source;
	EBook *book;
	EContact *contact;
	char *uri, *ebook_uri, *uid, *msg;
	GError *err = NULL;

	/* "peek" in a function name means we're getting back a
	 * pointer to one of the source_selector's internal variables,
	 * so we shouldn't unref it when we're done.
	 */
	source = e_source_selector_peek_primary_selection (source_selector);

	uri = e_source_get_uri (source);
	rss_fetcher = g_hash_table_lookup (priv->rss_fetchers, uri);
	if (rss_fetcher) {
		g_free (uri);
		goto got_fetcher;
	}

	/* The uri should look like:
	 * brainread://Group Name/ebook-uri#contact-uid
	 */
	if (strncmp (uri, "brainread://", 12) != 0) {
		g_free (uri);
		return;
	}

	ebook_uri = uri + sizeof ("brainread://");
	ebook_uri = strchr (ebook_uri, '/');
	if (!ebook_uri) {
		g_free (uri);
		return;
	}
	ebook_uri++;

	uid = strchr (ebook_uri, '#');
	if (!uid)
		return; /* FIXME */
	*uid = '\0';
	uid++;

	if (ebook_uri && *ebook_uri)
		book = e_book_new (source, NULL);
	else
		book = e_book_new_system_addressbook (&err);
	if (err) {
		e_notice (source_selector, GTK_MESSAGE_ERROR,
			  _("Could not get address book:\n%s"),
			  err->message);
		g_error_free (err);
		g_object_unref (book);
		return;
	}

	e_book_open (book, TRUE, &err);
	if (err) {
		e_notice (source_selector, GTK_MESSAGE_ERROR,
			  _("Could not open address book:\n%s"),
			  err->message);
		g_error_free (err);
		g_object_unref (book);
		return;
	}
	if (!e_book_get_contact (book, uid, &contact, &err)) {
		e_notice (source_selector, GTK_MESSAGE_ERROR,
			  _("Could not find contact:\n%s"),
			  err->message);
		g_error_free (err);
		g_object_unref (book);
		return;
	}
	g_object_unref (book);

	rss_fetcher = brainread_blog_fetcher_new (priv->session, contact);
	g_object_unref (contact);
	g_hash_table_insert (priv->rss_fetchers, uri, rss_fetcher);
	g_signal_connect (rss_fetcher, "fetched_data",
			  G_CALLBACK (fetched_data), view);

 got_fetcher:
	msg = g_strdup_printf (_("Loading %s..."),
			       brainread_rss_fetcher_url (rss_fetcher));
	e_activity_handler_set_message (priv->activity_handler, msg);
	g_free (msg);
	brainread_rss_fetcher_fetch (rss_fetcher);
}


struct fetch_image_data {
	BrainReadView *view;
	GtkHTML *html;
	GtkHTMLStream *handle;
};

static void
free_fid (gpointer user_data, GObject *ex_msg)
{
	struct fetch_image_data *fid = user_data;

	fid->view->priv->msgs = g_slist_remove (fid->view->priv->msgs, ex_msg);
	g_free (fid);
}

static void
got_image (SoupMessage *msg, gpointer user_data)
{
	struct fetch_image_data *fid = user_data;

	gtk_html_end (fid->html, fid->handle,
		      SOUP_STATUS_IS_SUCCESSFUL (msg->status_code) ?
		      GTK_HTML_STREAM_OK :
		      GTK_HTML_STREAM_ERROR);
}

static void
got_chunk (SoupMessage *msg, gpointer user_data)
{
	struct fetch_image_data *fid = user_data;

	gtk_html_write (fid->html, fid->handle,
			msg->response.body, msg->response.length);
}

static void
url_requested (GtkHTML *html, const char *url, GtkHTMLStream *handle,
	       gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;

	if (!strncmp (url, "data:;base64,", 13)) {
		GByteArray *image;

		/* This is a "data:" URL (for inline image data
		 * from the EContact record). Just decode it.
		 */
		image = brainread_base64_decode (url + 13);
		gtk_html_write (html, handle, image->data, image->len);
		g_byte_array_free (image, TRUE);
		gtk_html_end (html, handle, GTK_HTML_STREAM_OK);
	} else if (!strncmp (url, "http", 4)) {
		SoupMessage *msg;
		struct fetch_image_data *fid;

		/* Queue an async HTTP request */
		msg = soup_message_new ("GET", url);

		fid = g_new0 (struct fetch_image_data, 1);
		fid->view = view;
		fid->html = html;
		fid->handle = handle;
		soup_session_queue_message (priv->session, msg,
					    got_image, fid);
		/* Connect to the "got_chunk" signal so we can render
		 * the images incrementally.
		 */
		g_signal_connect (msg, "got_chunk",
				  G_CALLBACK (got_chunk), fid);

		/* Keep track of the message so we can free the data
		 * when we're done, or cancel it if the view is freed.
		 */
		g_object_weak_ref (G_OBJECT (msg), free_fid, fid);
		priv->msgs = g_slist_prepend (priv->msgs, msg);
	}
}


static void
link_clicked (GtkHTML *html, const char *url, gpointer user_data)
{
	GError *err = NULL;

	gnome_url_show (url, &err);
	if (err) {
		e_notice (html, GTK_MESSAGE_ERROR,
			  "%s", err->message);
		g_error_free (err);
	}
}

static void
on_url (GtkHTML *html, const char *url, gpointer user_data)
{
	BrainReadView *view = user_data;
	BrainReadViewPrivate *priv = view->priv;

	if (url && *url)
		e_activity_handler_set_message (priv->activity_handler, url);
	else
		e_activity_handler_unset_message (priv->activity_handler);
}


BrainReadView *
brainread_view_new (SoupSession *session, ESourceList *source_list)
{
	BrainReadView *view;
	BrainReadViewPrivate *priv;
	GtkWidget *widget, *scrolled;

	view = g_object_new (BRAINREAD_TYPE_VIEW,
			     "session", session,
			     "source_list", source_list,
			     NULL);
	priv = view->priv;

	/* Our sidebar is an ESourceSelector (from the evolution tree) */
	widget = e_source_selector_new (priv->source_list);
	gtk_widget_show (widget);
	priv->source_selector = E_SOURCE_SELECTOR (widget);

	/* Turn off checkboxes, connect to its signal */
	e_source_selector_show_selection (priv->source_selector, FALSE);
	g_signal_connect (priv->source_selector, "primary_selection_changed",
			  G_CALLBACK (source_selected), view);

	/* Now, create a BonoboControl wrapper for the sidebar.
	 * BonoboControl is a class that implements the Bonobo::Control
	 * CORBA interface for a widget.
	 */
	priv->sidebar_control = bonobo_control_new (widget);

	/* Now for the view, a GtkHTML */
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_widget_show (scrolled);
	widget = gtk_html_new ();
	gtk_widget_show (widget);
	gtk_container_add (GTK_CONTAINER (scrolled), widget);
	priv->html = GTK_HTML (widget);
	g_signal_connect (priv->html, "url_requested",
			  G_CALLBACK (url_requested), view);
	g_signal_connect (priv->html, "link_clicked",
			  G_CALLBACK (link_clicked), view);
	g_signal_connect (priv->html, "on_url",
			  G_CALLBACK (on_url), view);
	priv->view_control = bonobo_control_new (scrolled);

	/* Connect to the view control's "activate" signal so we know
	 * when it is activated and deactivated.
	 */
	g_signal_connect (priv->view_control, "activate",
			  G_CALLBACK (view_activated), view);

	priv->activity_handler = e_activity_handler_new ();
	widget = e_task_bar_new ();
	gtk_widget_show (widget);
	priv->task_bar = E_TASK_BAR (widget);
	e_activity_handler_attach_task_bar (priv->activity_handler,
					    priv->task_bar);
	priv->statusbar_control = bonobo_control_new (widget);

	return view;
}
