/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-blog-fetcher.c
 *
 * A subclass of BrainReadRSSFetcher that handles a single contact
 * whose blog we are tracking.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "brainread-blog-fetcher.h"
#include "brainread-utils.h"

#include <gal/util/e-util.h>
#include <libebook/e-contact.h>

struct BrainReadBlogFetcherPrivate {
	char *name;
	char *photo_url;
};

static void fetched_data (BrainReadRSSFetcher *rf, SoupMessage *msg,
			  BrainReadRSS *rss, GError *err);

#define PARENT_TYPE BRAINREAD_TYPE_RSS_FETCHER
static BrainReadRSSFetcherClass *parent_class = NULL;

static void
init (GObject *object)
{
	BrainReadBlogFetcher *bf = BRAINREAD_BLOG_FETCHER (object);

	bf->priv = g_new0 (BrainReadBlogFetcherPrivate, 1);
}

/* BrainReadBlogFetcher doesn't have its own dispose implementation,
 * because it doesn't reference any other objects itself.
 */

static void
finalize (GObject *object)
{
	BrainReadBlogFetcher *bf = BRAINREAD_BLOG_FETCHER (object);

	g_free (bf->priv->name);
	g_free (bf->priv->photo_url);
	g_free (bf->priv);

	/* This will call BrainReadRSSFetcher's finalize method
	 * (because "parent_class" points to the
	 * BrainReadRSSFetcherClass that
	 * brainread-rss-fetcher.c:class_init() initialized).
	 */
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
class_init (GObjectClass *object_class)
{
	BrainReadRSSFetcherClass *fetcher_class =
		BRAINREAD_RSS_FETCHER_CLASS (object_class);

	parent_class = g_type_class_peek_parent (object_class);

	/* Override some GObject methods */
	object_class->finalize = finalize;

	/* And a BrainReadRSSFetcher signal */
	fetcher_class->fetched_data = fetched_data;
}

E_MAKE_TYPE (brainread_blog_fetcher, "BrainReadBlogFetcher", BrainReadBlogFetcher, class_init, init, PARENT_TYPE)


/* This is our overridden default signal handler implementation
 * for "fetched_data". Because brainread-rss-fetcher.c declared
 * this signal to be "G_SIGNAL_RUN_FIRST", this function will
 * be called before any actual signal handlers are.
 */
static void
fetched_data (BrainReadRSSFetcher *rf, SoupMessage *msg,
	      BrainReadRSS *rss, GError *err)
{
	BrainReadBlogFetcher *bf = BRAINREAD_BLOG_FETCHER (rf);

	/* If we got RSS data with no image, and we have a
	 * photo from the EContact, substitute that in.
	 */
	if (rss && !rss->image_url && bf->priv->photo_url) {
		rss->image_url = g_strdup (bf->priv->photo_url);

		/* Fill in the other mandatory image fields */
		rss->image_link = g_strdup (rss->link);
		rss->image_title = g_strdup (bf->priv->name);
	}
}

/**
 * brainread_blog_fetcher_new:
 * @session: #SoupSession to use for HTTP
 * @contact: an #EContact
 *
 * Creates a new #BrainReadBlogFetcher based on @contact
 *
 * Return value: the new blog_fetcher
 **/
BrainReadRSSFetcher *
brainread_blog_fetcher_new (SoupSession *session, EContact *contact)
{
	BrainReadBlogFetcher *bf;
	const char *url;
	EContactPhoto *photo;

	/* Try the "Blog URL" field first, but fallback to the
	 * "Homepage URL" field if the blog field is empty.
	 */
	url = e_contact_get_const (contact, E_CONTACT_BLOG_URL);
	if (!url || !*url)
		url = e_contact_get_const (contact, E_CONTACT_HOMEPAGE_URL);
	if (!url || !*url)
		return NULL;

	bf = g_object_new (BRAINREAD_TYPE_BLOG_FETCHER,
			   "session", session,
			   "url", url,
			   NULL);

	bf->priv->name = e_contact_get (contact, E_CONTACT_FULL_NAME);

	/* If there's a picture associated with the contact, keep a copy
	 * of it. (At the time of writing [2004-03-24] there's no way
	 * to associate a picture with a contact, but this is supposed
	 * to be fixed eventually.)
	 */
	photo = e_contact_get (contact, E_CONTACT_PHOTO);
	if (photo) {
		char *base64photo;

		base64photo = brainread_base64_encode (photo->data, photo->length);
		bf->priv->photo_url =
			g_strdup_printf ("data:;base64,%s", base64photo);
		g_free (base64photo);
		e_contact_photo_free (photo);
	}

	/* We know bf can safely be cast to BrainReadRSSFetcher, so
	 * it's safe (and faster) to use a direct C typecast rather
	 * than "BRAINREAD_RSS_FETCHER (bf)".
	 */
	return (BrainReadRSSFetcher *)bf;
}
