/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-rss-fetcher.c
 *
 * Fetches and parses RSS feeds on demand.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <ctype.h>
#include <string.h>

#include "brainread-rss-fetcher.h"
#include "brainread-marshal.h"
#include "brainread-utils.h"

#include <gal/util/e-util.h>
#include <libgnome/gnome-i18n.h>
#include <libsoup/soup-session.h>
#include <libsoup/soup-uri.h>
#include <libxml/HTMLparser.h>

struct BrainReadRSSFetcherPrivate {
	SoupSession *session;
	char        *url;
	SoupMessage *pending_msg;
	gboolean     be_clever;
};

/* Signals */
enum {
	FETCHED_DATA,
	LAST_SIGNAL
};
static guint signals [LAST_SIGNAL] = { 0 };

/* Properties */
enum {
	PROP_SESSION = 1,
	PROP_RSS_URL,
	PROP_BE_CLEVER,
	LAST_PROP
};

/* Prototypes for the methods we implement. */
static void set_property (GObject *object, guint prop_id,
			  const GValue *value, GParamSpec *pspec);
static void get_property (GObject *object, guint prop_id,
			  GValue *value, GParamSpec *pspec);

static void fetch (BrainReadRSSFetcher *rf);

#define PARENT_TYPE G_TYPE_OBJECT
static GObjectClass *parent_class = NULL;

/* This is the initializer for new BrainReadRSSFetcher objects,
 * which gets called automatically as part of g_object_new().
 */
static void
init (GObject *object)
{
	BrainReadRSSFetcher *rf = BRAINREAD_RSS_FETCHER (object);

	rf->priv = g_new0 (BrainReadRSSFetcherPrivate, 1);
	rf->priv->be_clever = TRUE;
}

/* "dispose" is the early stage of object destruction. When dispose is
 * called, the object should unreference any other objects it is
 * currently holding references to (but not widgets, because they are
 * destroyed when their parent is destroyed). Beware that dispose may
 * be called multiple times during destruction.
 */
static void
dispose (GObject *object)
{
	BrainReadRSSFetcher *rf = BRAINREAD_RSS_FETCHER (object);

	if (rf->priv->pending_msg) {
		soup_message_set_status (rf->priv->pending_msg,
					 SOUP_STATUS_CANCELLED);
		soup_session_cancel_message (rf->priv->session,
					     rf->priv->pending_msg);
		rf->priv->pending_msg = NULL;
	}

	if (rf->priv->session) {
		g_object_unref (rf->priv->session);
		rf->priv->session = NULL;
	}

	/* Chain to the parent class's implementation. (This
	 * is an important idiom in the GNOME object system.)
	 */
	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/* "finalize" is the very last stage of object destruction, in
 * which the object must free any remaining memory it has allocated.
 */
static void
finalize (GObject *object)
{
	BrainReadRSSFetcher *rf = BRAINREAD_RSS_FETCHER (object);

	g_free (rf->priv->url);
	g_free (rf->priv);

	/* Chain to the parent class's implementation. Eventually this
	 * will reach GObjectClass's finalize method, which will free
	 * the object itself.
	 */
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/* This initializes the BrainReadComponentClass object,
 * setting up default method implementations, signal handlers,
 * etc. Each of the class's ancestor classes will also have
 * their class_init function called on the class object,
 * starting with GObject, and working its way down to your
 * class_init. (In this case, it would be just GObject's
 * and then this one.) So this means each subclass has the
 * opportunity to override the methods set up by its
 * parent classes.
 */
static void
class_init (GObjectClass *object_class)
{
	BrainReadRSSFetcherClass *fetcher_class =
		BRAINREAD_RSS_FETCHER_CLASS (object_class);

	/* Get a pointer to our parent class (which will be used
	 * to chain to the parent implementations of dispose
	 * and finalize.
	 */
	parent_class = g_type_class_peek_parent (object_class);

	/* Override some GObject methods */
	object_class->dispose = dispose;
	object_class->finalize = finalize;
	object_class->set_property = set_property;
	object_class->get_property = get_property;

	/* Initialize our own virtual method */
	fetcher_class->fetch = fetch;

	/* Define our signal. Again mostly boilerplate. */
	signals[FETCHED_DATA] =
		g_signal_new ("fetched_data",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (BrainReadRSSFetcherClass, fetched_data),
			      NULL, NULL,
			      brainread_marshal_NONE__OBJECT_POINTER_POINTER,
			      G_TYPE_NONE, 3,
			      SOUP_TYPE_MESSAGE,
			      G_TYPE_POINTER,
			      G_TYPE_POINTER);

	/* Define our properties. Properties are special because
	 * they can be introspected by the program and the type
	 * system. Among other things, this makes them much easier
	 * to handle when writing a wrapper for the class in
	 * another programming language. Properties also give a
	 * standard way to pass construct-time data to an object.
	 */
	g_object_class_install_property (
		object_class, PROP_SESSION,
		g_param_spec_object ("session", "Session",
				     "The SoupSession to use for HTTP",
				     SOUP_TYPE_SESSION,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (
		object_class, PROP_RSS_URL,
		g_param_spec_string ("url", "RSS URL",
				     "The URL of the RSS feed to fetch",
				     NULL,
				     G_PARAM_READWRITE));
	g_object_class_install_property (
		object_class, PROP_BE_CLEVER,
		g_param_spec_boolean ("be_clever", "Be clever",
				      "Whether or not to try to parse HTML looking for RSS",
				      TRUE,
				      G_PARAM_READWRITE));
}

/* This is a macro (defined in <gal/util/e-util.h>) that creates the
 * brainread_rss_fetcher_get_type function for us, saving us from
 * a little bit of additional cut+pastage.
 */
E_MAKE_TYPE (brainread_rss_fetcher, "BrainReadRSSFetcher", BrainReadRSSFetcher, class_init, init, PARENT_TYPE)

/* More boilerplate-ish code, to implement getting and setting our
 * properties.
 */
static void
set_property (GObject *object, guint prop_id,
	      const GValue *value, GParamSpec *pspec)
{
	BrainReadRSSFetcher *rf = BRAINREAD_RSS_FETCHER (object);

	switch (prop_id) {
	case PROP_SESSION:
		/* Since this is construct-only, we know rf->priv->session
		 * must currently be NULL, so we don't have to worry
		 * about unreffing it.
		 */
		rf->priv->session = SOUP_SESSION (g_value_dup_object (value));
		break;
	case PROP_RSS_URL:
		if (rf->priv->url)
			g_free (rf->priv->url);
		rf->priv->url = g_value_dup_string (value);

		/* Maybe we should check if there's a request pending
		 * and cancel it if so?
		 */

		break;
	default:
		break;
	}
}

static void
get_property (GObject *object, guint prop_id,
	      GValue *value, GParamSpec *pspec)
{
	BrainReadRSSFetcher *rf = BRAINREAD_RSS_FETCHER (object);

	switch (prop_id) {
	case PROP_SESSION:
		g_value_set_object (value, rf->priv->session);
		break;
	case PROP_RSS_URL:
		g_value_set_string (value, rf->priv->url);
		break;
	default:
		break;
	}
}


/**
 * brainread_rss_fetcher:
 * @session: the #SoupSession to use for HTTP requests
 * @rss_url: the URL of the RSS feed to fetch
 *
 * Creates a new #BrainReadRSSFetcher object.
 *
 * Return value: the #BrainReadRSSFetcher
 **/
BrainReadRSSFetcher *
brainread_rss_fetcher_new (SoupSession *session, const char *rss_url)
{
	return g_object_new (BRAINREAD_TYPE_RSS_FETCHER,
			     "session", session,
			     "url", rss_url,
			     NULL);
}


/* Utility function used to decide if from_url and to_url point
 * to the same domain or not.
 */
static gboolean
same_domain (const char *from_url, const char *to_url)
{
	char *p;
	int len;

	/* If to_url is relative, it's the same domain as from_url
	 * basically by definition.
	 */
	if (!strchr (to_url, ':'))
		return TRUE;

	/* If it's not http, it's no good */
	if (g_ascii_strncasecmp (to_url, "http://", 7) != 0 &&
	    g_ascii_strncasecmp (to_url, "https://", 8) != 0)
		return FALSE;

	/* Otherwise find the domain part of from_url and compare
	 * to to_url.
	 */
	p = strstr (from_url, "://");
	g_return_val_if_fail (p != NULL, FALSE);
	p = strchr (p + 3, '/');
	if (p)
		len = p - from_url;
	else {
		/* Must be something like "http://foo.com" */
		len = strlen (from_url);
	}

	return g_ascii_strncasecmp (from_url, to_url, len) == 0;
}	

/* utility function to get the last pathname component of a URL */
static xmlChar *
url_basename (xmlChar *url)
{
	xmlChar *p;

	p = strrchr (url, '/');
	if (p)
		return p + 1;
	else
		return url;
}

#define non_word_char(x) ((x) > 127 || !isalpha ((unsigned char)(x)))

/* Search @body looking for something that looks like an RSS URL. */
static xmlChar *
find_rss_link (const char *source_url, const char *body, int length)
{
	char *body_copy;
	xmlDoc *doc;
	xmlNode *link, *img;
	xmlChar *href, *prop, *text, *p;

	/* Unfortunately, libxml doesn't have an HTML parser interface
	 * that takes a buffer and a length, so we have to copy the
	 * response body into a '\0'-terminated string.
	 */
	body_copy = g_malloc (length + 1);
	memcpy (body_copy, body, length);
	body_copy[length] = '\0';

	doc = htmlParseDoc (body_copy, NULL);
	g_free (body_copy);

	if (!doc) {
		/* Must have not been HTML */
		return NULL;
	}

	/* The "official" way to find an RSS feed is to look for
	 * <link rel="alternate" type="application/rss+xml"
	 *       title="RSS" href="..." />
	 *
	 * brainread_xml_find_next() is explained fully in
	 * brainread-utils.c.
	 */
	link = doc->children;
	while ((link = brainread_xml_find_next (link, NULL, "link", TRUE))) {
		prop = xmlGetProp (link, "rel");
		if (!prop || g_ascii_strcasecmp (prop, "alternate") != 0) {
			xmlFree (prop);
			continue;
		}
		xmlFree (prop);

		prop = xmlGetProp (link, "type");
		if (!prop || g_ascii_strcasecmp (prop, "application/rss+xml") != 0) {
			xmlFree (prop);
			continue;
		}
		xmlFree (prop);

		prop = xmlGetProp (link, "title");
		if (!prop || g_ascii_strcasecmp (prop, "RSS") != 0) {
			xmlFree (prop);
			continue;
		}
		xmlFree (prop);

		href = xmlGetProp (link, "href");
		if (href)
			return href;
	}

	/* If that doesn't work, we try to find an explicit
	 * <a href="..."> link in the page that looks like it's
	 * probably a link to an RSS feed. First we try for a link to
	 * "rss.xml" or "*.rss*"
	 */
	link = doc->children;
	while ((link = brainread_xml_find_next (link, NULL, "a", TRUE))) {
		href = xmlGetProp (link, "href");
		if (!href)
			continue;
		if (!same_domain (source_url, href)) {
			xmlFree (href);
			continue;
		}

		p = url_basename (href);
		if (!g_ascii_strcasecmp (p, "rss.xml"))
			return href;

		p = strrchr (p, '.');
		if (p && !g_ascii_strncasecmp (p, ".rss", 4))
			return href;

		xmlFree (href);
	}

	/* Failing that, we look at the content of the links, looking
	 * for one of:
	 *   - the word "RSS" (case insensitive)
	 *   - the word "syndicate" (case insensitive) as the first word
	 *   - <img src="xml.gif"> (the widely-used little orange button)
	 */
	link = doc->children;
	while ((link = brainread_xml_find_next (link, NULL, "a", TRUE))) {
		href = xmlGetProp (link, "href");
		if (!href)
			continue;
		if (!same_domain (source_url, href)) {
			xmlFree (href);
			continue;
		}

		text = xmlNodeGetContent (link);
		if (text) {
			if (!g_ascii_strncasecmp (text, "syndicate", 9) &&
			    non_word_char (text[9])) {
				xmlFree (text);
				return href;
			}

			for (p = text; *p; p++) {
				if (!g_ascii_strncasecmp (p, "rss", 3) &&
				    (p == text || non_word_char (p[-1])) &&
				    non_word_char (p[3])) {
					xmlFree (text);
					return href;
				}
			}
			xmlFree (text);
		}

		img = link;
		while ((img = brainread_xml_find_next (img, link, "img", TRUE))) {
			prop = xmlGetProp (img, "src");
			if (!prop)
				continue;

			if (g_ascii_strcasecmp (url_basename (prop), "xml.gif") == 0) {
				xmlFree (prop);
				return href;
			}
			xmlFree (prop);
		}

		xmlFree (href);
	}

	/* Oh well. Give up. */
	return NULL;
}

/* Given a SoupMessage that doesn't contain an RSS feed, see if we
 * can find a better URL.
 */
static gboolean
try_fix_url (BrainReadRSSFetcher *rf, SoupMessage *msg)
{
	const char *content_type;
	xmlChar *href;
	const SoupUri *base_uri;
	SoupUri *new_uri;
	char *new_uri_str, *source_url;

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code))
		return FALSE;

	/* If the data is explicitly marked as being something other
	 * than HTML, ignore it.
	 */
	content_type = soup_message_get_header (msg->response_headers,
						"Content-Type");
	if (content_type &&
	    g_ascii_strncasecmp (content_type, "text/html", 9) != 0 &&
	    g_ascii_strncasecmp (content_type, "text/xhtml+xml", 14) != 0)
		return FALSE;

	/* OK, let's try... */
	base_uri = soup_message_get_uri (msg);
	source_url = soup_uri_to_string (base_uri, FALSE);
	href = find_rss_link (source_url,
			      msg->response.body, msg->response.length);
	g_free (source_url);
	if (!href)
		return FALSE;

	/* Success! But the href may be relative... */
	new_uri = soup_uri_new_with_base (base_uri, href);
	new_uri_str = soup_uri_to_string (new_uri, FALSE);

	/* Update our URL */
	g_object_set (G_OBJECT (rf),
		      "url", new_uri_str,
		      NULL);
	g_free (new_uri_str);
	soup_uri_free (new_uri);
	xmlFree (href);

	return TRUE;
}

/* Callback for soup_session_queue_message. Called when either a
 * response has been received or the message has failed due to some
 * transport problem. (Eg, "can't connect to host".)
 */
static void
got_rss_feed (SoupMessage *msg, gpointer user_data)
{
	BrainReadRSSFetcher *rf = user_data;
	BrainReadRSS *rss;
	GError *err = NULL;

	rf->priv->pending_msg = NULL;

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) {
		g_set_error (&err, BRAINREAD_RSS_FETCHER_ERROR_HTTP,
			     msg->status_code,
			     _("Could not retrieve RSS feed: %s"),
			     msg->reason_phrase);
		rss = NULL;
	} else {
		rss = brainread_rss_new (msg->response.body,
					 msg->response.length,
					 FALSE, &err);

		/* If we didn't get RSS data, try (once) to be clever
		 * and find an RSS link in the HTML data.
		 */
		if (!rss && rf->priv->be_clever) {
			rf->priv->be_clever = FALSE;
			if (try_fix_url (rf, msg)) {
				/* We think we found something, so try
				 * again.
				 */
				brainread_rss_fetcher_fetch (rf);
				return;
			}
		}
	}

	/* Now emit our "fetched_data" signal so that whoever asked
	 * us to fetch the data can do what they need to do with it.
	 */
	g_signal_emit (rf, signals[FETCHED_DATA], 0, msg, rss, err);

	if (err)
		g_error_free (err);
	if (rss)
		brainread_rss_free (rss);
}

static void
fetch (BrainReadRSSFetcher *rf)
{
	/* If we're already fetching, we don't need to do anything */
	if (rf->priv->pending_msg)
		return;

	/* Create a new soup message to get the contents of the URL
	 * and queue it to be sent asynchronously. Soup will call
	 * "got_rss_feed" when the response comes back.
	 */
	rf->priv->pending_msg = soup_message_new ("GET", rf->priv->url);
	soup_session_queue_message (rf->priv->session, rf->priv->pending_msg,
				    got_rss_feed, rf);
}

/**
 * brainread_rss_fetcher_fetch:
 * @rf: the RSS fetcher
 *
 * Asks @rf to asynchronously fetch RSS data from its associated
 * RSS feed. It will emit a %fetched_data signal when it has the
 * data (or if an error occurs).
 **/
void
brainread_rss_fetcher_fetch (BrainReadRSSFetcher *rf)
{
	/* Try to protect against crashing in the event of a
	 * programmer error. If either of these fail, they will
	 * spew a warning to stderr and then return. Note the
	 * explicit "!= NULL" in the second one, which is used
	 * because it makes the "assertion failed" error message
	 * more understandable.
	 */
	g_return_if_fail (BRAINREAD_IS_RSS_FETCHER (rf));
	g_return_if_fail (rf->priv->url != NULL);

	/* This function is just a wrapper for a virtual method. It
	 * should not do anything except call the virtual method,
	 * because if it does, then authors of language bindings
	 * (for python, C++, etc) will need to special-case the
	 * method.
	 *
	 * Since we set the BrainReadRSSFetcherClass's "fetch"
	 * member to point to the "fetch" function above, that is
	 * what will be called (unless this is being called on
	 * a subclass of BrainReadRSSFetcher, and that subclass
	 * overrode the method).
	 */

	BRAINREAD_RSS_FETCHER_GET_CLASS (rf)->fetch (rf);
}

const char *
brainread_rss_fetcher_url (BrainReadRSSFetcher *rss_fetcher)
{
	g_return_val_if_fail (BRAINREAD_IS_RSS_FETCHER (rss_fetcher), NULL);

	return rss_fetcher->priv->url;
}


GQuark
brainread_rss_fetcher_error_quark (void)
{
	static GQuark error_quark;

	if (!error_quark)
		error_quark = g_quark_from_static_string ("BRAINREAD_RSS_FETCHER_ERROR");
	return error_quark;
}

