/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Copyright (C) 2004 Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* brainread-component.c
 *
 * This declares a BonoboObject subclass that implements the
 * GNOME::Evolution::Component CORBA interface, which allows it to act
 * as an Evolution shell component. brainread-component-factory.c
 * handles registering with bonobo-activation, and creates a
 * BrainReadComponent when one is requested by the shell.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include "brainread-component.h"
#include "brainread-source-utils.h"
#include "brainread-utils.h"
#include "brainread-view.h"

#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-exception.h>
#include <e-util/e-dialog-utils.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>
#include <libsoup/soup-session-async.h>
#include <shell/evolution-shell-component-utils.h>

#define d(x)

#define PARENT_TYPE bonobo_object_get_type ()
static BonoboObjectClass *parent_class = NULL;

struct BrainReadComponentPrivate {
	SoupSession      *session;
	ESourceList      *source_list;
};

/* GObject methods */

static void
dispose (GObject *object)
{
	BrainReadComponentPrivate *priv = BRAINREAD_COMPONENT (object)->priv;

	if (priv->session) {
		g_object_unref (priv->session);
		priv->session = NULL;
	}
	if (priv->source_list) {
		g_object_unref (priv->source_list);
		priv->source_list = NULL;
	}

	/* Chain to the parent class's implementation */
	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
finalize (GObject *object)
{
	BrainReadComponentPrivate *priv = BRAINREAD_COMPONENT (object)->priv;

	g_free (priv);

	/* Chain to the parent class's implementation. Eventually this
	 * will reach GObjectClass's finalize method, which will free
	 * the object itself.
	 */
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/* Evolution::Component CORBA methods. */

static void
impl_upgradeFromVersion (PortableServer_Servant servant,
			 const CORBA_short major,
			 const CORBA_short minor,
			 const CORBA_short revision,
			 CORBA_Environment *ev)
{
	/* This is the first version, so there won't be anything
	 * to upgrade regardless of what version the user is coming
	 * from.
	 */
}

static CORBA_boolean
impl_requestQuit (PortableServer_Servant servant,
		  CORBA_Environment *ev)
{
	/* The shell calls this to ask each component if it's OK
	 * to quit. The mailer would return FALSE if there was a
	 * composer window open, for instance. We can always
	 * return TRUE.
	 */
	return TRUE;
}

static CORBA_boolean
impl_quit (PortableServer_Servant servant,
	   CORBA_Environment *ev)
{
	/* Likewise, we never have any state to save, so we can
	 * just quit.
	 */
	return TRUE;
}

static GNOME_Evolution_CreatableItemTypeList *
impl__get_userCreatableItems (PortableServer_Servant servant,
			      CORBA_Environment *ev)
{
	GNOME_Evolution_CreatableItemTypeList *list = GNOME_Evolution_CreatableItemTypeList__alloc ();

	list->_length  = 1;
	list->_maximum = list->_length;
	list->_buffer  = GNOME_Evolution_CreatableItemTypeList_allocbuf (list->_length);

	CORBA_sequence_set_release (list, FALSE);

	list->_buffer[0].id = "blogger";
	list->_buffer[0].description = _("New Blog Feed");
	list->_buffer[0].menuDescription = _("_Blog Feed");
	list->_buffer[0].tooltip = _("Add a new blog feed");
	list->_buffer[0].menuShortcut = 'b';
	list->_buffer[0].iconName = BRAINREAD_ICONDIR "/brainread-mini.png";
	list->_buffer[0].type = GNOME_Evolution_CREATABLE_OBJECT;

	return list;
}

static void
impl_requestCreateItem (PortableServer_Servant servant,
			const CORBA_char *item_type_name,
			CORBA_Environment *ev)
{
	BrainReadComponent *component =
		BRAINREAD_COMPONENT (bonobo_object_from_servant (servant));
	ESourceGroup *blogs_group;

	if (strcmp (item_type_name, "blogger") != 0)
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, ex_GNOME_Evolution_Component_Failed, NULL);

	/* Evolution doesn't currently have a widget to let the user
	 * pick a group from an ESourceList, so we just only work with
	 * one group for now.
	 */
	blogs_group = e_source_list_peek_group_by_name (component->priv->source_list, _("Blogs"));
	g_return_if_fail (blogs_group != NULL);

	brainread_source_add (NULL, blogs_group);
}

static void
impl_createControls (PortableServer_Servant servant,
		     Bonobo_Control *corba_sidebar_control,
		     Bonobo_Control *corba_view_control,
		     Bonobo_Control *corba_statusbar_control,
		     CORBA_Environment *ev)
{
	BrainReadComponent *component =
		BRAINREAD_COMPONENT (bonobo_object_from_servant (servant));
	BrainReadComponentPrivate *priv = component->priv;
	BrainReadView *view;

	view = brainread_view_new (priv->session, priv->source_list);
	*corba_sidebar_control = CORBA_Object_duplicate (BONOBO_OBJREF (brainread_view_peek_sidebar (view)), ev);
	*corba_view_control = CORBA_Object_duplicate (BONOBO_OBJREF (brainread_view_peek_view (view)), ev);
	*corba_statusbar_control = CORBA_Object_duplicate (BONOBO_OBJREF (brainread_view_peek_statusbar (view)), ev);

	/* FIXME: view gets leaked */
}

static void
brainread_component_class_init (BrainReadComponentClass *klass)
{
	POA_GNOME_Evolution_Component__epv *epv = &klass->epv;
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	/* Override some methods in one of our parent classes. */
	object_class->dispose = dispose;
	object_class->finalize = finalize;

	/* Declare implementations for the GNOME::Evolution::Component
	 * methods.
	 */
	epv->upgradeFromVersion = impl_upgradeFromVersion;
	epv->createControls = impl_createControls;
	epv->requestQuit = impl_requestQuit;
	epv->quit = impl_quit;
	epv->_get_userCreatableItems = impl__get_userCreatableItems;
	epv->requestCreateItem = impl_requestCreateItem;
}

static void
brainread_component_init (BrainReadComponent *component)
{
	BrainReadComponentPrivate *priv;

	priv = g_new0 (BrainReadComponentPrivate, 1);
	component->priv = priv;

	priv->session = soup_session_async_new ();
	priv->source_list = brainread_source_list_new ();
}

/* BONOBO_TYPE_FUNC_FULL is defined in <bonobo/bonobo-object.h>.
 * It expands out to define brainread_component_get_type() for us,
 * filling in all of the boilerplate. (PARENT_TYPE was defined
 * above to be BONOBO_TYPE_OBJECT.)
 */
BONOBO_TYPE_FUNC_FULL (BrainReadComponent, GNOME_Evolution_Component, PARENT_TYPE, brainread_component)
