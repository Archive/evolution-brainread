/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* Copyright (C) 2004 Novell, Inc. */

#ifndef __BRAINREAD_VIEW_H__
#define __BRAINREAD_VIEW_H__

/* This header is mostly GObject-related boilerplate. See
 * brainread-rss-fetcher.h for more information.
 */

#include <bonobo/bonobo-control.h>
#include <libedataserver/e-source-list.h>
#include <libsoup/soup-session.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define BRAINREAD_TYPE_VIEW            (brainread_view_get_type ())
#define BRAINREAD_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), BRAINREAD_TYPE_VIEW, BrainReadView))
#define BRAINREAD_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), BRAINREAD_TYPE_VIEW, BrainReadViewClass))
#define BRAINREAD_IS_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BRAINREAD_TYPE_VIEW))
#define BRAINREAD_IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), BRAINREAD_TYPE_VIEW))

typedef struct BrainReadView           BrainReadView;
typedef struct BrainReadViewPrivate    BrainReadViewPrivate;
typedef struct BrainReadViewClass      BrainReadViewClass;

struct BrainReadView {
	GObject parent;

	BrainReadViewPrivate *priv;
};

struct BrainReadViewClass {
	GObjectClass parent_class;

};

GType          brainread_view_get_type       (void);

BrainReadView *brainread_view_new            (SoupSession *session,
					      ESourceList *source_list);

BonoboControl *brainread_view_peek_sidebar   (BrainReadView *view);
BonoboControl *brainread_view_peek_view      (BrainReadView *view);
BonoboControl *brainread_view_peek_statusbar (BrainReadView *view);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BRAINREAD_VIEW_H__ */
